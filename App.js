import React, { Component } from 'react';
import { View, Text,TextInput,ScrollView,Button,alert } from 'react-native';
// import auth from '@react-native-firebase/auth'
import FirebaseSetup from './setupPhone'
const {auth} =  FirebaseSetup() 
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone:'',
      confirm:'',
      codeconfirm:''
    };
  }


    // If null, no SMS has been sent
    // const [confirm, setConfirm] = useState(null);
  
    // const [code, setCode] = useState('');
  
    // Handle the button press
     signInWithPhoneNumber = ()=>{
      const confirmation =  auth().signInWithPhoneNumber(this.state.phone);
      // setConfirm(confirmation);
      console.warn(confirmation)
      this.setState({codeconfirm:confirmation})
    }
  
  
    confirmCode = ()=>{
      console.warn('press')
      try {
         confirm.confirm(this.state.confirm);
         alert('User signin successful')
      } catch (error) {
        console.warn('Invalid code.');
      }
    }

  render() {
    if(this.state.codeconfirm == ''){
      return (
        <View style={{ flex:1,paddingHorizontal:10,paddingVertical:10 }}>
          <Text style={{ textAlign:'center',marginTop:25 }}> Phone Verify </Text>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ marginTop:20, }}>
                  <TextInput
                    style={{ backgroundColor:'#fff',borderWidth:1,borderColor:'#ddd',fontSize:14,paddingVertical:20,paddingHorizontal:20,fontFamily:'Roboto-Regular', }}
                    placeholder="phone number"
                    keyboardType='phone-pad'
                    value={this.state.phone}
                    autoCapitalize="none"
                    onChangeText={(text)=>this.setState({phone:text})}
                  />
              </View>
              <View style={{ marginTop:20 }}>
                <Button
                  onPress={()=>this.signInWithPhoneNumber()}
                  title="send"
                  color="green"
                  accessibilityLabel="Learn more about this purple button"
                />
              </View>
          </ScrollView>
        </View>
      );
    }else{
      return (
        <View style={{ flex:1,paddingHorizontal:10,paddingVertical:10 }}>
          <Text style={{ textAlign:'center',marginTop:25 }}> Phone Verify </Text>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ marginTop:20, }}>
                  <TextInput
                    style={{ backgroundColor:'#fff',borderWidth:1,borderColor:'#ddd',fontSize:14,paddingVertical:20,paddingHorizontal:20,fontFamily:'Roboto-Regular', }}
                    placeholder="Confirm code"
                    keyboardType="default"
                    value={this.state.confirm}
                    autoCapitalize="none"
                    onChangeText={(text)=>this.setState({confirm:text})}
                  />
              </View>
              <View style={{ marginTop:20 }}>
                <Button
                  onPress={()=>this.confirmCode()}
                  title="send"
                  color="green"
                  accessibilityLabel="Learn more about this purple button"
                />
              </View>
          </ScrollView>
        </View>
      );
    }
  }
}

export default App;



// import React, { useState, useEffect } from 'react';
// import {
//   Button, TextInput, View, Text, StyleSheet,
//   TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard
// } from 'react-native';
// import auth from '@react-native-firebase/auth';
// import FirebaseSetup from './setupPhone';
// const {firebase} =  FirebaseSetup() 
// export default function AuthAndInfo({ navigation }) {
//   // If null, no SMS has been sent
//   const [confirm, setConfirm] = useState(null);

//   const [code, setCode] = useState('');
//   const [address, setAddress] = useState('');

//   const [inputPhone, setInputPhone] = useState('');

//   const [initializing, setInitializing] = useState(true);
//   const [user, setUser] = useState();

//   // Handle user state changes
//   function onAuthStateChanged(user) {
//     setUser(user);
//     if (initializing) setInitializing(false);
//   }

//   useEffect(() => {
//     const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
//     return subscriber; // unsubscribe on unmount
//   }, []);

//   // Handle the button press
//   async function signInWithPhoneNumber(phoneNumber) {
//     const confirmation = await auth().signInWithPhoneNumber('+88' + phoneNumber);
//     setConfirm(confirmation);
//   }

//   async function confirmCode() {
//     try {
//       await confirm.confirm(code);
//       const { currentUser } = firebase.auth();
//       console.log('current user', currentUser);
//     } catch (error) {
//       console.log('Invalid code.');
//     }
//   }

//   if (user) {
//     var ref = firebase.database().ref('/users/' + user.phoneNumber);
//     ref.set({ phone: user.phoneNumber, address: address });
//     navigation.navigate('Cart');
//     return (
//       <View>
//         <Text>{user.phoneNumber}</Text>
//       </View>
//     );
//   } else {
//     if (!confirm) {
//       return (

//         <KeyboardAvoidingView
//           behavior={Platform.OS == "ios" ? "padding" : "height"}
//           style={styles.container}
//         >
//           <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

//             <View style={styles.inner}>
//               <TextInput
//                 keyboardType="numeric"
//                 onChangeText={(phoneN) => setInputPhone(phoneN)}
//                 placeholder="Phone Number e.g. 0171...."
//                 style={styles.textInput} />

//               <TextInput
//                 onChangeText={(address) => setAddress(address)}
//                 placeholder="Shipping Address"
//                 style={styles.textInput} />

//               <View style={styles.btnContainer}>
//                 <Button title="Submit" onPress={() => signInWithPhoneNumber(inputPhone)} />
//               </View>
//             </View>



//           </TouchableWithoutFeedback>

//         </KeyboardAvoidingView>


//       );
//     } else {
//       return (
//         <>
//           <Text>Input OTP code</Text>
//           {/* <TextInput value={code} onChangeText={(text) => setCode(text)} /> */}
//           <View style={{ marginBottom: 20 }}>
//             <TextInput
//               autoFocus={true}
//               keyboardType="numeric"
//               onChangeText={(text) => setCode(text)}
//               style={{
//                 backgroundColor: '#f5f4f2',
//                 fontWeight: '600',
//                 alignSelf: 'center',
//                 alignItems: 'center',
//                 textAlign: 'center',
//                 fontSize: 20,
//                 height: 55,
//                 width: '50%',
//                 borderRadius: 10,
//                 borderWidth: 0.5,
//                 borderColor: 'grey',
//               }}
//             />

//           </View>
//           <Button title="Confirm" onPress={() => confirmCode()} />
//         </>
//       );
//     }
//   }
// }


// const styles = StyleSheet.create({
//   container: {
//     flex: 1
//   },
//   inner: {
//     padding: 24,
//     flex: 1,
//     justifyContent: "space-around"
//   },
//   textInput: {

//     backgroundColor: '#f5f4f2',
//     fontWeight: '600',
//     alignSelf: 'center',
//     alignItems: 'center',
//     textAlign: 'center',
//     fontSize: 20,
//     height: 55,
//     width: '100%',
//     borderRadius: 10,
//     borderWidth: 0.5,
//     borderColor: 'grey',
//   },

//   btnContainer: {
//     backgroundColor: "white",
//     marginTop: 12
//   }
// });