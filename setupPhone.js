import  firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth'
const FirebaseConfig = {
  apiKey: "AIzaSyCT-AaICSLT0RtJP1ylu3H0ZdNrqsyz-Lk",
  authDomain: "phoneverify-c3af5.firebaseapp.com",
  projectId: "phoneverify-c3af5",
  storageBucket: "phoneverify-c3af5.appspot.com",
  messagingSenderId: "493811863840",
  appId: "1:493811863840:web:337af3e238f3eb774a0745",
  measurementId: "G-FN5BRE9G1V"
}

if( !firebase.apps.length){
  firebase.initializeApp(FirebaseConfig)
}

export default ()=>{
    return { 
        firebase,auth
    }
}